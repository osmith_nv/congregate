import requests
from requests.auth import HTTPBasicAuth

from congregate.helpers.exceptions import ConfigurationException
from congregate.helpers.conf import Config
from congregate.migration.gitlab.api.groups import GroupsApi
from congregate.migration.gitlab.api.users import UsersApi
from congregate.helpers.misc_utils import is_error_message_present, safe_json_response
from congregate.helpers.utils import is_github_dot_com
from congregate.helpers.json_utils import json_pretty


class ConfigurationValidator(Config):
    '''
    Extended config class used to validate the configuration on run
    '''

    def __init__(self, path=None):
        self.groups = GroupsApi()
        self.users = UsersApi()
        self._dstn_parent_id_validated_in_session = False
        self._import_user_id_validated_in_session = False
        self._dstn_parent_group_path_validated_in_session = False
        self._dstn_token_validated_in_session = False
        self._src_token_validated_in_session = False
        super().__init__(path=path)

    @property
    def dstn_parent_id(self):
        dstn_parent_id = self.prop_int("DESTINATION", "dstn_parent_group_id")
        if self.dstn_parent_id_validated_in_session:
            return dstn_parent_id
        self.dstn_parent_id_validated_in_session = self.validate_dstn_parent_group_id(
            dstn_parent_id)
        if self.dstn_parent_group_path_validated_in_session:
            return dstn_parent_id
        self.dstn_parent_group_path_validated_in_session = self.validate_dstn_parent_group_path(
            self.prop("DESTINATION", "dstn_parent_group_path"))
        return dstn_parent_id

    @property
    def import_user_id(self):
        import_user_id = self.prop_int("DESTINATION", "import_user_id")
        if self.import_user_id_validated_in_session:
            return import_user_id
        self.import_user_id_validated_in_session = self.validate_import_user_id(
            import_user_id)
        return import_user_id

    @property
    def dstn_parent_group_path(self):
        dstn_parent_group_path = self.prop(
            "DESTINATION", "dstn_parent_group_path")
        if self.dstn_parent_group_path_validated_in_session:
            return dstn_parent_group_path
        self.dstn_parent_group_path_validated_in_session = self.validate_dstn_parent_group_path(
            dstn_parent_group_path)
        return dstn_parent_group_path

    @property
    def destination_token(self):
        dstn_token = self.prop(
            "DESTINATION", "dstn_access_token", default=None, obfuscated=True)
        if self.dstn_token_validated_in_session:
            return dstn_token
        self.dstn_token_validated_in_session = self.validate_dstn_token(
            dstn_token)
        return dstn_token

    @property
    def source_token(self):
        src_token = self.prop("SOURCE", "src_access_token",
                              default=None, obfuscated=True)
        if self.src_token_validated_in_session:
            return src_token
        self.src_token_validated_in_session = self.validate_src_token(
            src_token)
        return src_token

    def validate_dstn_parent_group_id(self, pgid):
        if pgid is not None:
            group_resp = safe_json_response(self.groups.get_group(
                pgid, self.destination_host, self.destination_token))
            error, group_resp = is_error_message_present(group_resp)
            if error or not group_resp:
                raise ConfigurationException("parent_id")
            return True
        return True

    def validate_import_user_id(self, iuid):
        if iuid is not None:
            user_resp = safe_json_response(self.users.get_user(
                iuid, self.destination_host, self.destination_token))
            error, user_resp = is_error_message_present(user_resp)
            if error or not user_resp:
                raise ConfigurationException("import_user_id")
            elif user_resp.get("error") is not None:
                if user_resp["error"] == "invalid_token":
                    raise ConfigurationException(
                        "parent_token", msg=f"{json_pretty(user_resp)}")
                raise Exception(user_resp)
            if user_resp["id"] == iuid:
                return True
        raise ConfigurationException("import_user_id")

    def validate_dstn_parent_group_path(self, dstn_parent_group_path):
        if dstn_parent_group_path is not None:
            group_resp = safe_json_response(self.groups.get_group(
                self.prop_int("DESTINATION", "dstn_parent_group_id"),
                self.destination_host,
                self.destination_token))
            error, group_resp = is_error_message_present(group_resp)
            if error or not group_resp:
                raise ConfigurationException(
                    "dstn_parent_group_path", msg=f"Invalid dest parent group param:\n{json_pretty(group_resp)}")
            elif group_resp["full_path"] == dstn_parent_group_path:
                return True
        return True

    def validate_dstn_token(self, dstn_token):
        if dstn_token is not None:
            user = safe_json_response(self.users.get_current_user(
                self.destination_host, dstn_token))
            error, user = is_error_message_present(user)
            if error or not user or not user.get("is_admin"):
                raise ConfigurationException(
                    "destination_token", msg=f"Invalid user and/or token:\n{json_pretty(user)}")
            return True
        return True

    def validate_src_token(self, src_token):
        if src_token:
            user = None
            err_msg = "Invalid user and/or token:\n"
            if self.source_type == "gitlab":
                user = safe_json_response(
                    self.users.get_current_user(self.source_host, src_token))
                error, user = is_error_message_present(user)
                if not user or error or not user.get("is_admin"):
                    raise ConfigurationException(
                        "source_token", msg=f"{err_msg}{json_pretty(user)}")
            elif self.source_type == "github":
                user = safe_json_response(requests.get(
                    f"{self.source_host.rstrip('/')}/user" if is_github_dot_com(
                        self.source_host) else f"{self.source_host}/api/v3/user",
                    params={},
                    headers={
                        "Accept": "application/vnd.github.v3+json",
                        "Authorization": f"token {src_token}"
                    },
                    verify=self.ssl_verify))
                error, user = is_error_message_present(user)
                if error or not user or (not user.get("site_admin") and not is_github_dot_com(self.source_host)):
                    raise ConfigurationException(
                        "source_token", msg=f"{err_msg}{json_pretty(user)}")
            elif self.source_type == "bitbucket server":
                user = safe_json_response(requests.get(
                    f"{self.source_host}/rest/api/1.0/admin/permissions/users?filter={self.source_username}",
                    params={},
                    headers={
                        "Content-Type": "application/json"
                    },
                    auth=HTTPBasicAuth(self.source_username, src_token),
                    verify=self.ssl_verify
                )
                )
                not_sys_admin = user["values"][0]["permission"] not in ["SYS_ADMIN", "ADMIN"] if user.get(
                    "values", []) else True
                error, user = is_error_message_present(user)
                if not user or error or not_sys_admin:
                    raise ConfigurationException(
                        "source_token", msg=f"{err_msg}{json_pretty(user)}")
            return True
        return True

    @property
    def dstn_parent_id_validated_in_session(self):
        return self._dstn_parent_id_validated_in_session

    @property
    def import_user_id_validated_in_session(self):
        return self._import_user_id_validated_in_session

    @property
    def dstn_parent_group_path_validated_in_session(self):
        return self._dstn_parent_group_path_validated_in_session

    @property
    def dstn_token_validated_in_session(self):
        return self._dstn_token_validated_in_session

    @property
    def src_token_validated_in_session(self):
        return self._src_token_validated_in_session

    @dstn_parent_id_validated_in_session.setter
    def dstn_parent_id_validated_in_session(self, value):
        self._dstn_parent_id_validated_in_session = value

    @import_user_id_validated_in_session.setter
    def import_user_id_validated_in_session(self, value):
        self._import_user_id_validated_in_session = value

    @dstn_parent_group_path_validated_in_session.setter
    def dstn_parent_group_path_validated_in_session(self, value):
        self._dstn_parent_group_path_validated_in_session = value

    @dstn_token_validated_in_session.setter
    def dstn_token_validated_in_session(self, value):
        self._dstn_token_validated_in_session = value

    @src_token_validated_in_session.setter
    def src_token_validated_in_session(self, value):
        self._src_token_validated_in_session = value

from time import sleep
import requests
from requests.auth import HTTPBasicAuth

from congregate.helpers.base_class import BaseClass
from congregate.helpers.decorators import stable_retry


class BitBucketServerApi(BaseClass):

    def generate_bb_v1_request_url(self, api, branch_permissions=False):
        if branch_permissions:
            return f"{self.config.source_host}/rest/branch-permissions/2.0/{api}"
        return f"{self.config.source_host}/rest/api/1.0/{api}"

    def generate_v4_request_headers(self):
        return {
            "Content-Type": "application/json"
        }

    def get_authorization(self):
        return HTTPBasicAuth(self.config.source_username, self.config.source_token)

    @stable_retry
    def generate_get_request(self, api, url=None, params=None, branch_permissions=False):
        """
        Generates GET request to BitBucket API.
        You will need to provide the access token, and specific api url.

            :param token: (str) Access token to BitBucket instance
            :param api: (str) Specific BitBucket API endpoint (ex: projects)
            :param url: (str) A URL to a location not part of the BitBucket API. Defaults to None
            :param params:
            :return: The response object *not* the json() or text()
        """

        if url is None:
            url = self.generate_bb_v1_request_url(
                api, branch_permissions=branch_permissions)

        headers = self.generate_v4_request_headers()

        if params is None:
            params = {}

        auth = self.get_authorization()
        return requests.get(url, params=params, headers=headers, auth=auth, verify=self.config.ssl_verify)

    def list_all(self, api, params=None, limit=1000, branch_permissions=False):
        isLastPage = False
        start = 0
        self.log.info("Listing endpoint: {}".format(api))
        while isLastPage is False:
            if not params:
                params = {
                    "start": start,
                    "limit": limit
                }
            else:
                params["start"] = start
                params["limit"] = limit
            r = self.generate_get_request(
                api, params=params, branch_permissions=branch_permissions)
            try:
                data = r.json()
                self.log.info("Retrieved {0} {1}".format(
                    data.get("size", None), api))
                if r.status_code != 200:
                    if r.status_code == 404 or r.status_code == 500:
                        self.log.error('\nERROR: HTTP Response was {}\n\nBody Text: {}\n'.format(
                            r.status_code, r.text))
                        break
                    raise ValueError('ERROR HTTP Response was NOT 200, which implies something wrong. The actual return code was {}\n{}\n'.format(
                        r.status_code, r.text))
                if data.get("values", None) is not None:
                    for value in data["values"]:
                        yield value
                    isLastPage = data['isLastPage']
                    if isLastPage is False:
                        start = data['nextPageStart']
                else:
                    isLastPage = True
            except ValueError as e:
                self.log.error(e)
                self.log.error("API Request didn't return JSON")
                # Retry interval is smaller here because it will just retry
                # until it succeeds
                self.log.info("Attempting to retry after 3 seconds")
                sleep(3)
